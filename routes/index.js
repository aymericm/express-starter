var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');


/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Express ft MongoDB boilerplate',
        page_title: 'Home page'
    });
});

router.get('/all', function(req, res) {
    var MongoClient = mongodb.MongoClient;

    var url = "mongodb://localhost:27017/mongo";

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Failed to connect', err);
        } else {
            console.log('Connected !');

            var collection = db.collection('students');

            collection.find({}).toArray(function(err, result) {
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    res.render('studentlist', {
                        studentlist: result,
                        page_title: "Student list"
                    });
                } else {
                    res.send('No documents found');
                }

                db.close();
            });
        }
    });
});


router.get('/new', function(req, res) {
    res.render('new', {
        page_title: "New Student"
    });
});

router.post('/addstudent', function(req, res) {
    var MongoClient = mongodb.MongoClient;

    var url = "mongodb://localhost:27017/mongo";

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to server', err);
        } else {
            console.log('Connected !');

            var collection = db.collection('students');
            var student1 = {
                name: req.body.name,
                username: req.body.username,
                email: req.body.email,
                address: {
                    street: req.body.street,
                    city: req.body.city
                }
            };

            collection.insert([student1], function(err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.redirect('all');
                }
            });
            db.close();
        }
    });
});

module.exports = router;
